1. Install
   ```sh
   npm install
   ```
2. Runing Applications
   ```sh
   npm run start
   ```

3. Run Test And Lint

   ```
    npm run lint && npm run test && npm run e2e
   ```