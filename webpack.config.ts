import path from 'path';

import { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import Dotenv from 'dotenv-webpack';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import  FaviconsWebpackPlugin from 'favicons-webpack-plugin';

interface Configuration extends WebpackConfiguration {
  devServer?: WebpackDevServerConfiguration;
}

process.stdout.write(`Building for ${process.env.NODE_ENV}...\n`);

const cssRegex = /\.css$/;
const cssModuleRegex = /\.module\.css$/;
const isProduction = process.env.NODE_ENV === 'production';

const plugins = [
  new HtmlWebpackPlugin({
    template: 'index.html',
  }),
  new FaviconsWebpackPlugin({
    logo: './src/assets/favicon.png',
    mode: 'webapp', 
    devMode: 'webapp', 
    favicons: {
      appName: 'Todo Applications',
      appDescription: 'Todo Applications',
      background: '#fff',
      theme_color: '#fff'
    }
  }),
  new Dotenv(),
  new BundleAnalyzerPlugin(),
  ...(isProduction
    ? [
        {
          apply: (compiler: any) => {
            compiler.hooks.done.tap('DonePlugin', () => {
              console.log('Compile is done !');
              setTimeout(() => {
                process.exit(0);
              });
            });
          },
        },
      ]
    : []),
] as Configuration['plugins'];

const configuration: Configuration = {
  entry: './src/index.tsx',
  target: 'web',
  mode: isProduction ? 'production' : 'development',
  devtool: isProduction ? 'source-map' : 'eval-cheap-module-source-map',
  devServer: {
    port: 8080,
    historyApiFallback: true,
    open: true,
  },
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
    filename: '[name].bundle.js',
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.(j|t)sx?$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: cssRegex,
        exclude: cssModuleRegex,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
          },
        ],
      },
      {
        test: cssModuleRegex,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: {
                mode: 'local',
                localIdentName: '[name]__[local]--[hash:base64:5]',
              },
            },
          },
        ],
      }
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.jsx', '.js'],
    modules: ['node_modules', 'src'],
  },
  plugins,
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },
};

export default configuration;