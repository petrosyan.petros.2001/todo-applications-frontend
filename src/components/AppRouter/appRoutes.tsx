import React from 'react';
import { RouteObject } from 'react-router';

import { SignIn } from 'pages/sign-in';
import { SignUp } from 'pages/sign-up';

export const appRoutes: RouteObject[] = [
  {
    path: '/sign-in',
    element: <SignIn />,
  },
  {
    path: '/sign-up',
    element: <SignUp />,
  }
];
