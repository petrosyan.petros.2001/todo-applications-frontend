import React, { useCallback } from 'react';
import { Form, Input, Button, Row, Col } from 'antd';
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';

export const SignUp: React.FC = () => {
  const navigate = useNavigate()
  const onSubmit = useCallback((values)=>{
    console.log(values)
  }, [])

  const handleRedirect = useCallback(() =>{
    navigate('/sign-in')
  },[navigate]);

  return (
    <Row className='full-screen flex justify-content-center align-items-center'>
      <Col span={8}>
        <h2 className='title'>Welcome To Todo Application</h2>
        <Form
          name='normal_login'
          className='login-form'
          initialValues={{
            email: '',
            password: '',
          }}
          onFinish={onSubmit}
        >
          <Form.Item
            name='email'
            rules={[
              {
                type: 'email',
                message: 'The input is not valid Email!',
              },
              {
                required: true,
                message: 'Please input your Email!',
              },
            ]}
          >
            <Input
              prefix={<MailOutlined className='site-form-item-icon' />}
              placeholder='Email'
            />
          </Form.Item>
          <Form.Item
            name='password'
            rules={[
              {
                required: true,
                message: 'Please input your Password!',
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className='site-form-item-icon' />}
              type='password'
              placeholder='Password'
            />
          </Form.Item>
          <Form.Item
            name='re-password'
            rules={[
              {
                required: true,
                message: 'Please input Retype Password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    'The two passwords that you entered do not match!'
                  );
                },
              }),
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className='site-form-item-icon' />}
              type='password'
              placeholder='Retype Password'
            />
          </Form.Item>
          <Form.Item>
            <Row className='justify-content-center'>
              <Col>
                <Button
                  type='primary'
                  htmlType='submit'
                  className='login-form-button'
                >
                  Sign Up
                </Button>
                Or <a onClick={handleRedirect}>Sign In</a>
              </Col>
            </Row>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
