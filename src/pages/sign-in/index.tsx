import React, { useCallback } from 'react';
import { Form, Input, Button, Row, Col } from 'antd';
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';

export const SignIn: React.FC = () => {
  const navigate = useNavigate()
  const onSubmit = useCallback((values)=>{
    console.log(values)
  }, [])

  const handleRedirect = useCallback(() =>{
    navigate('/sign-up')
  },[navigate]);

  return (
    <Row className='full-screen flex justify-content-center align-items-center'>
      <Col sm={12} md={12} lg={8} xs={16}>
        <h2 className='title'>Welcome To Todo Application</h2>
        <Form
          name='normal_login'
          className='login-form'
          initialValues={{
            email: '',
            password: '',
          }}
          onFinish={onSubmit}
        >
          <Form.Item
            name='email'
            rules={[
              {
                type: 'email',
                message: 'The input is not valid Email!',
              },
              {
                required: true,
                message: 'Please input your Email!',
              },
            ]}
          >
            <Input
              prefix={<MailOutlined className='site-form-item-icon' />}
              placeholder='Email'
            />
          </Form.Item>
          <Form.Item
            name='password'
            rules={[
              {
                required: true,
                message: 'Please input your Password!',
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className='site-form-item-icon' />}
              type='password'
              placeholder='Password'
            />
          </Form.Item>
          <Form.Item>
            <Row className='justify-content-center'>
              <Col>
                <Button
                  type='primary'
                  htmlType='submit'
                  className='login-form-button'
                >
                  Sign In
                </Button>
                Or <a onClick={handleRedirect}>Sign Up</a>
              </Col>
            </Row>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
