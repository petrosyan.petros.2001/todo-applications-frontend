import React from 'react';
import 'antd/dist/antd.css';
import "./global.css"
import ReactDOM from 'react-dom';
import axios from "axios";
import App from './App';
import { apiClient } from 'utils/http';

const rootElement = document.getElementById('root');
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  ReactDOM.render(<App />, rootElement);
} else {
  const configUrl = '/config.json';

  console.log('Get configuration from ', configUrl);

  axios
    .get(configUrl)
    .then(response => {
      const { api_url } = response.data;
      apiClient.defaults.baseURL = api_url;
      return <App />;
    })
    .catch(e => {
      console.log(
        `Something horribly broke while getting the config for a reason ${e}`
      );
      return (
        <div>
          The application is not running. Please contact technical support.
          Error info: {e.toString()}
        </div>
      );
    })
    .then(app => {
      ReactDOM.render(app, rootElement);
    });
}