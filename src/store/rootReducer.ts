import { combineReducers } from 'redux';

import { accountSlice } from 'store/slices/account/accountSlice';

const rootReducer = combineReducers({
  account: accountSlice.reducer,
});

export default rootReducer;
