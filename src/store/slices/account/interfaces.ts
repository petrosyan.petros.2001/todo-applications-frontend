
export interface IAccountState {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  isEmailConfirmed: boolean;
  jwt: {
    iat: number;
    exp: number;
  };
}
