export enum ApiErrors {
    USER_NOT_FOUND = 'User with specified email address not found',
    DUPLICATED_EMAIL = 'This email address is already registered',
    PASSWORD_INCORRECT = 'Invalid email or password',
    PASSWORD_EMAIL_INCORRECT = 'Invalid email or password'
  }
  